// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
const vscode = require('vscode');
const {
	randomEnText, 
	randomCnText, 
	randomParagraph, 
	randomCnParagraph,
	randomEnWord,
	randomTime,
	randomUuid,
  randomPhone,
  randomAddress,
  randomEmail,
  randomUrl,
  randomUsername
} = require('./utils/randomtext.js');
// This method is called when your extension is activated
// Your extension is activated the very first time the command is executed

/**
 * 插入文本
 * @param {String} text 
 * @returns 
 */
const insertTextAction = (text) => {
		let editor = vscode.window.activeTextEditor;
		if (!editor) {
			return;
		}
		let selection = editor.selection;
		// @ts-ignore
		editor.edit(editBuilder => {
			// @ts-ignore
			editBuilder.insert(selection.start, text);
		});
}

/**
 * 插入代码片段
 * @param {String} snippetString
 * @returns 
 */
const insertSnippetAction = (snippetString) => {
	let editor = vscode.window.activeTextEditor;
	if (!editor) {
		return;
	}
	let selection = editor.selection;
	editor.insertSnippet(new vscode.SnippetString(snippetString), selection.start);
}

/**
 * @param {vscode.ExtensionContext} context
 */
function activate(context) {

	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log('Congratulations, your extension "placeholder" is now active!');

	// The command has been defined in the package.json file
	// Now provide the implementation of the command with  registerCommand
	// The commandId parameter must match the command field in package.json
	let selectPick = '';

	let disposable = vscode.commands.registerCommand('placeholder.placeholder', function () {
		// The code you place here will be executed every time your command is executed
		// let editor = vscode.window.activeTextEditor;
		// editor.insertSnippet(new vscode.SnippetString('我们是朋友123456世上只有妈妈好！'))
		// console.log('---- placeholder.helloWorld ----:', editor.edit);
		vscode.window.showQuickPick([{
				label: '中文',
				description: '随机中文',
				detail: '插入指定个数的中文汉字'
			},{
				label: '中文段落',
				description: '随机中文段落',
				detail: '插入指定个数的中文段落'
			}, {
				label: '图片',
				description: '随机图片',
				detail: '插入随机占位图片'
			}, {
				label: '英文',
				description: '随机英文',
				detail: '插入指定个数的英文单词'
			}, {
				label: '英文字母',
				description: '随机英文字母',
				detail: '插入指定个数的英文字母'
			}, {
				label: '英文段落',
				description: '随机英文段落',
				detail: '插入指定个数的英文段落'
		}, {
			label: '其它',
			description: '其它特定格式占位符',
			detail: 'uuid,时间、手机号、邮箱、地址、用户名、url等占位符'
		}]).then(value => {
				if (!value) {
					return;
				}
				vscode.window.showInformationMessage('你选择插入占位：' + value.label);
				selectPick = value.label;
				if (selectPick === '图片') {
						const settings = vscode.workspace.getConfiguration('placeholder');
						const snippetImgs = {
							Picsum: {
								text: 'https://picsum.photos/${1:300}/${2:300}.${3|jpg,webp|}?random='+Date.now(),
								img: `<img src="https://picsum.photos/\${1:300}/\${2:300}.\${3|jpg,webp|}?random=${Date.now()}" alt="">`
							},
							FPOImg: {
								text: 'https://fpoimg.com/${1:300}x${2:300}?text=${3:example}&bg_color=${4:999999}&text_color=${5:ff4400}',
								img: '<img src="https://fpoimg.com/${1:300}x${2:300}?text=${3:example}&bg_color=${4:999999}&text_color=${5:ff4400}" alt="">'
							},
							DummyImage: {
								text: 'https://dummyimage.com/${1:300}x${2:300}/${3:999999}/${4:ff4400}.${5|png,jpg,gif|}&text=${6:EXAMPLE}',
								img: '<img src="https://dummyimage.com/${1:300}x${2:300}/${3:999999}/${4:ff4400}.${5|png,jpg,gif|}&text=${6:EXAMPLE}" alt="">'
							},
							ViaPlaceholder: {
								text: 'https://via.placeholder.com/${1:640}x${2:300}/${3:999999}/${4:ff4400}.${5|png,jpg,gif,webp|}?text=${6:example}',
								img: '<img src="https://via.placeholder.com/${1:640}x${2:300}/${3:999999}/${4:ff4400}.${5|png,jpg,gif,webp|}?text=${6:example}" alt="">'
							},
							PlaceholdJp: {
								text: 'https://placehold.jp/${1:999999}/${2:ff4400}/${3:300}x${4:300}.${5|png,jpg,gif,webp|}?text=${6:EXAMPLE}',
								img: '<img src="https://placehold.jp/${1:999999}/${2:ff4400}/${3:300}x${4:300}.${5|png,jpg,gif,webp|}?text=${6:EXAMPLE}" alt="">'
							},
							DevtoolTech: {
								text: 'https://devtool.tech/api/placeholder/${1:300}/${2:300}?text=${3:示例图片}&color=${4:#ffffff}&bgColor=${5:#333333}',
								img: '<img src="https://devtool.tech/api/placeholder/${1:300}/${2:300}?text=${3:示例图片}&color=${4:#ffffff}&bgColor=${5:#333333}" alt="">'
							},
						}
						vscode.window.showQuickPick([{
								label: 'Picsum',
								description: 'Picsum占位图片',
								detail: '图片有实际内容，格式jpg/webp'
							},{
								label: 'DummyImage',
								description: 'DummyImage占位图',
								detail: '纯色图片，格式png/jpg/gif,支持显示文字（不支持中文）'
							},{
								label: 'PlaceholdJp',
								description: 'placehold.jp占位图片',
								detail: '纯色图片，格式png/jpg/gif/webp,支持显示文字（支持中文）'
							},{
								label: 'DevtoolTech',
								description: 'DevtoolTech占位图',
								detail: '纯色图片，不支持定义格式,支持显示文字（支持中文）'
							},{
								label: 'ViaPlaceholder',
								description: 'ViaPlaceholder点位图',
								detail: '纯色图片，格式png/jpg/gif/webp,支持显示文字（不支持中文）'
							},{
								label: 'FPOImg',
								description: 'FPOImg占位图片',
								detail: '纯色图片，不支持定义格式,支持显示文字（不支持中文）'
							}]).then((imgValue) => {
								if (!imgValue) {
									return;
								}
								vscode.window.showInformationMessage('你选择插入占位：' + imgValue.description);
								const imgType = settings.get('img');
								let snippetString = snippetImgs[imgValue.label][imgType === 'url' ? 'text' : 'img'];
								insertSnippetAction(snippetString);
							})
						return;
				}
				if (selectPick === '其它') {
					vscode.window.showQuickPick([{
							label: 'uuid',
							description: '随机uuid',
							detail: '插入一个随机uuid'
						}, {
							label: '日期',
							description: '随机日期',
							detail: '插入一个随机日期'
						},{
							label: '电话',
							description: '随机电话号码',
							detail: '插入随机电话号码'
						}, {
							label: '邮箱',
							description: '随机邮箱',
							detail: '插入随机邮箱'
						}, {
							label: '地址',
							description: '随机地址',
							detail: '插入一个随机地址'
						}, {
							label: '姓名',
							description: '随机姓名',
							detail: '插入随机姓名'
						}, {
							label: '网址',
							description: '随机网址',
							detail: '插入一个随机url网址'
					}]).then((otherValue) => {
						if (!otherValue) {
							return;
						}
						vscode.window.showInformationMessage('你选择插入占位：' + otherValue.label);
						let selectPick = otherValue.label;
						// 生成占位文本
						let insertText = '';
						switch(selectPick) {
							case 'uuid':
								insertText = randomUuid();
								break;
							case '日期':
								insertText = randomTime();
								break;
							case '电话':
								insertText = randomPhone();
								break;
							case '邮箱':
								insertText = randomEmail();
								break;
							case '地址':
								insertText = randomAddress();
								break;
							case '姓名':
								insertText = randomUsername();
								break;
							case '网址':
								insertText = randomUrl();
								break;
							default:
								break;
						}
						console.log("随机文本: 1", insertText);
						insertTextAction(insertText);
					})
					return;
				}
				// 输入框提示
				let tipsText = '';
				switch(selectPick) {
					case '中文':
						tipsText = '输入想生成的中文汉字个数';
						break;
					case '英文':
						tipsText = '输入想生成的英文单词个数';
						break;
					case '英文字母':
						tipsText = '输入想生成的英文字母个数';
						break;
					case '中文段落':
						tipsText = '输入想生成的中文段落数（一个。表示一段）';
						break;
					case '英文段落':
						tipsText = '输入想生成的英文段落数（一个.表示一段）';
						break;
					default:
						break;
				}
				// 占位文字
				vscode.window.showInputBox({ // 这个对象中所有参数都是可选参数
						password:false, // 输入内容是否是密码
						ignoreFocusOut:true, // 默认false，设置为true时鼠标点击别的地方输入框不会消失
						placeHolder: tipsText, // 在输入框内的提示信息
						prompt:'请输入数字', // 在输入框下方的提示信息
						validateInput:function(text){return Number(text) > 0 ? null : '请输入大于1的数字';} // 对输入内容进行验证并返回
					}).then(function(count){
						// 生成占位文本
						let insertText = '';
						switch(selectPick) {
							case '中文':
								insertText = randomCnText(count);
								break;
							case '英文':
								insertText = randomEnText(count);
								break;
							case '英文字母':
								insertText = randomEnWord(count);
								break;
							case '中文段落':
								insertText = randomCnParagraph(count);
								break;
							case '英文段落':
								insertText = randomParagraph(count);
								break;
							default:
								break;
						}
						console.log("随机文本：0", count, insertText);
						insertTextAction(insertText);
				});
			})
	});

	context.subscriptions.push(disposable);
}

// This method is called when your extension is deactivated
function deactivate() {}

module.exports = {
	activate,
	deactivate
}
